#!/usr/bin/env python

#    Author: Andrew Watts
#
#    Copyright 2010 Andrew Watts and the University of Rochester
#    BCS Department
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License version
#    2.1 as published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.
#    If not, see <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>.

"""
Pays out bonuses and approves work based on a <expt-name>-assignments.csv
file created by parseResults2.py.  Reads bonus information from a config file, of
the form: 

[Experiments]
<expt-name>=<bonus-amount>
<second-expt>=<bonus-amount-2>

To approve work only, set <bonus-amount> to 0 or simply ommit (the
equals sign is required, though).

This uses boto to connect to Amazon, which expects your credentials
(id and secret key) in a cfg file either in /etc/boto.cfg or ~/.boto.
(See the boto docs online for details).
"""

from __future__ import print_function

from boto.mturk.connection import MTurkConnection
from boto.mturk.price import Price
from boto.exception import AWSConnectionError, EC2ResponseError

import csv, sys, ConfigParser

if __name__ == '__main__':
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print('Usage: {0} <cfgfile> [-dryrun]'.format(__file__))
        sys.exit(1)

    cfg = ConfigParser.SafeConfigParser()
    cfg.read('../hits/{0}'.format(sys.argv[1]))

    dryrun = len(sys.argv) > 2
    if dryrun:
        print('Dry run! (nothing accepted/rejected, nothing paid)')

    # get list of experiments from the config file
    experiments = cfg.items('Experiment')

    try:
        if not dryrun:
            conn = MTurkConnection(is_secure=True)
            mtbal = conn.get_account_balance()
            if len(mtbal) == 1:
                print( "Available balance: ", mtbal[0])
            elif len(mtbal) == 2:
                print( "Available balance: ", mtbal[0])
                print( "OnHold balance: ", mtbal[1])
            avbal = mtbal[0]

        for expt_name, bonus_str in experiments:
            # get bonus amount, setting to 0 if not a valid 
            try:
                bonus_amt = float(bonus_str)
            except ValueError as e:
                bonus_amt = None
            print("Experiment {0}: bonus of {1}".format(expt_name, bonus_amt))
            with open(expt_name + '-assignments.csv', 'r') as csvinfile:
                bonuses = csv.DictReader(csvinfile)
                for row in bonuses:
                    price = Price(amount=bonus_amt)
                    worker_id = row['workerid']
                    assignment_id = row['assignmentid']
                    print("Worker {0}, assignment {1}:".format(worker_id, assignment_id))
                    # check to see if this entry has already been approved etc.
                    if row['reviewed']=='Approved':
                        print("  Work already approved (assignment status: {0})\n".format(row['reviewed']))
                        continue
                    else:
                        if row['accept']:
                            try:
                                if row['accept'] == 'yes':
                                    if not dryrun:
                                        conn.approve_assignment(assignment_id)
                                    print("  Assignment accepted...")
                                elif row['accept'] == 'rej':
                                    if row['feedback']:
                                        if not dryrun:
                                            conn.reject_assignment(assignment_id, row['feedback'])
                                        print("  Assignment rejected ({0})!".format(row['feedback']))
                                    else:
                                        if not dryrun:
                                            conn.reject_assignment(assignment_id)
                                        print("  Assignment rejected!")
                            except EC2ResponseError as e:
                                print (("Error!\n\tStatus: %s\n\tReason: %s\n\tBody: %s") % (e.status, e.reason, e.body))
                                continue
                        if row['bonus'] and bonus_amt > 0:
                            try:
                                if not dryrun:
                                    conn.grant_bonus(worker_id, assignment_id, price, "For completing whole experiment")
                                print("  Bonus awarded ({0})".format(bonus_amt))
                            except EC2ResponseError as e:
                                print (("Error!\n\tStatus: %s\n\tReason: %s\n\tBody: %s") % (e.status, e.reason, e.body))
                        else:
                            print("  NO BONUS AWARDED")
                            
    except AWSConnectionError as reason:
        print("Connection problem: ", reason)
        import sys
        sys.exit()
